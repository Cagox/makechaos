import { observer } from "mobx-react";
import * as userStore from "../store/userStore";
import clsx from "clsx";

const Message = observer(({ sender, message, time }) => {
  return (
    <div className="w-full relative mb-8">
      <div
        className={clsx("message max-w-xs break-words inline-block px-10")}
        style={{ overflow: "auto" }}
      >
        <div
          className={clsx(
            " bg-gray-500 text-white px-4 py-1 rounded-lg",
            sender
              ? sender == userStore.username && "bg-blue-500"
              : "bg-gray-600"
          )}
        >
          <p
            className={clsx(
              "text-lg"
              //   sender == userStore.username && "font-semibold"
            )}
          >
            {sender ? (sender == userStore.username ? "Siz" : sender) : "Gizli"}
          </p>
          <p> {message} </p>
        </div>

        <p className="text-right mr-3 mt-1">Cuma - 15:32</p>
      </div>
    </div>
  );
});

export default Message;
