export default function SignInput({
  placeholder,
  isPassword,
  theRef,
  enterFunc,
}) {
  return (
    <input
      ref={theRef}
      type={isPassword ? "password" : "text"}
      onKeyPress={(e) => {
        if (e.key == "Enter") {
          enterFunc();
        }
      }}
      className="w-full text-xl py-2 px-2 rounded-md mb-3"
      style={{
        outline: "none",
        border: "1px solid rgb(180,180,180)",
        color: "rgb(110,110,110)",
      }}
      placeholder={placeholder}
    />
  );
}
