import { observable, action, autorun } from "mobx";

// export const deneme = observable({ username: "sa" });
// export const deneme2 = observable.box("sasa").get();

// export const setters = action(() => {
//   deneme.username = "asdas";
// });

import * as firebase from "firebase/app";
import "firebase/auth";
import "firebase/firestore";

if (!firebase.apps.length) {
  firebase.initializeApp({
    apiKey: "AIzaSyBKWbyzAeXh6slQDcygvrcKKBYcmBQND-o",
    authDomain: "thefirst-611b6.firebaseapp.com",
    databaseURL: "https://thefirst-611b6.firebaseio.com",
    projectId: "thefirst-611b6",
    storageBucket: "thefirst-611b6.appspot.com",
    messagingSenderId: "431491878614",
    appId: "1:431491878614:web:cb01905ce52b7d61110377",
    measurementId: "G-JXWYTNN6B3",
  });
}

const isAuthF = async () => {
  await firebase.auth().onAuthStateChanged((user) => {
    if (user) {
      console.log("logged in");
      console.log(user.email);
      email.set(user.email);
      firebase
        .firestore()
        .collection("users")
        .doc(user.uid)
        .get()
        .then((data) => {
          if (data.data()) username.set(data.data().username);
        });
      isAuth.set(true);
      return;
    } else {
      console.log("logged out");
      isAuth.set(false);
      return;
    }
  });
};

export const isAuth = observable.box(isAuthF());
export const username = observable.box(null);
export const email = observable.box(null);
