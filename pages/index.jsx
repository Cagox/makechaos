import React, { useState, useRef, useCallback } from "react";
import Head from "next/head";
import Link from "next/link";
import { observer } from "mobx-react";
import SignInput from "../components/SignInput";
import * as userStore from "../store/userStore";

import * as firebase from "firebase/app";
import "firebase/auth";

const auth = firebase.auth();

const Home = observer(() => {
  const email = useRef(null);
  const password = useRef(null);

  const [signErr, setSignErr] = useState(null);

  const signin = useCallback(() => {
    auth
      .signInWithEmailAndPassword(email.current.value, password.current.value)
      .then(() => {
        window.location = "/home";
      })
      .catch((err) => {
        setSignErr("Email veya şifre hatalı");
      });
  }, [signErr]);

  if (typeof window !== "undefined" && userStore.isAuth.get() == true) {
    window.location = "/home";
  }

  if (!userStore.isAuth.get())
    return (
      <div style={{ background: "rgb(250,250,250)", height: "100vh" }}>
        <div className="w-9/12 xl:w-7/12 mx-auto">
          <Head>
            <title>MakeChaos</title>
            <link rel="icon" href="/favicon.ico" />
          </Head>

          <main className="container flex">
            <div className="w-1/2 py-48">
              <h1 className="text-5xl mb-0">MakeChaos</h1>
              <h3 className="text-3xl font-medium">
                Kimliğini gizle ve kaos yarat!
              </h3>
            </div>
            <div className="w-1/2 py-24">
              <div className="bg-white shadow-lg rounded-lg px-4 py-4">
                <SignInput
                  isPassword={false}
                  placeholder="Email Adresi"
                  theRef={email}
                  enterFunc={signin}
                />
                <SignInput
                  isPassword={true}
                  placeholder="Şifre"
                  theRef={password}
                  enterFunc={signin}
                />

                {signErr != null ? (
                  <p
                    className="error -mt-1 mb-2 pl-2 text-red-700 text-lg"
                    id="error"
                  >
                    {signErr}
                  </p>
                ) : null}

                <button
                  style={{ outline: "none" }}
                  className="w-full p-1 px-2 rounded-md bg-blue-500 text-white font-semibold text-2xl select-none hover:bg-blue-600 duration-300 transition-colors"
                  onClick={signin}
                >
                  Giriş Yap
                </button>

                <p
                  style={{ color: "rgb(60,60,60)" }}
                  className="text-center my-6 text-xl"
                >
                  ya da
                </p>

                <Link href="/signup">
                  <button
                    style={{
                      outline: "none",
                    }}
                    className="w-56 block mx-auto p-1 px-2 rounded-md bg-blue-600 text-white font-semibold text-2xl select-none hover:bg-blue-500 duration-300 transition-colors"
                  >
                    Kayıt Ol
                  </button>
                </Link>
              </div>
            </div>
          </main>
        </div>
      </div>
    );
});

export default Home;
