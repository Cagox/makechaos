import Head from "next/head";
import Link from "next/link";
import React, { useState, useRef, useCallback } from "react";
import * as userStore from "../store/userStore";

import SignInput from "../components/SignInput";

// This import loads the firebase namespace along with all its type information.
import * as firebase from "firebase/app";

// These imports load individual services into the firebase namespace.
import "firebase/auth";
import "firebase/firestore";

const auth = firebase.auth();

export default function Home() {
  const name = useRef(null);
  const username = useRef(null);
  const email = useRef(null);
  const password = useRef(null);
  const repassword = useRef(null);

  const [signErr, setSignErr] = useState(null);

  const validate = useCallback(() => {
    // console.log(name.current.value);
    // console.log(username.current.value);
    // console.log(email.current.value);
    // console.log(password.current.value);
    // console.log(repassword.current.value);

    const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    if (name.current.value === undefined || name.current.value.length < 4) {
      setSignErr("İsim en az 5 karakter olmalıdır");
    } else if (
      username.current.value === undefined ||
      username.current.value.length < 4
    ) {
      setSignErr("Kullanıcı Adı en az 5 karakter olmalıdır");
    } else if (!re.test(email.current.value)) {
      setSignErr("Geçersiz email");
    } else if (password.current.value.length < 6) {
      setSignErr("Parola en az 7 karakter olmalıdır");
    } else if (password.current.value !== repassword.current.value) {
      setSignErr("Parolalar eşleşmelidir");
    } else {
      auth
        .createUserWithEmailAndPassword(
          email.current.value,
          password.current.value
        )
        .then((data) => {
          firebase
            .firestore()
            .collection("users")
            .doc(data.user.uid)
            .set({ name: name.current.value, username: username.current.value })
            .then(() => {
              window.location = "/home";
            });
        })
        .catch((err) => {
          if (err.code === "auth/email-already-in-use") {
            setSignErr("Bu email adresi kullanımda");
          }
        });
    }
  }, [signErr]);

  if (typeof window !== "undefined" && userStore.isAuth.get() == true) {
    window.location = "/home";
  }

  if (!userStore.isAuth.get())
    return (
      <div style={{ background: "rgb(250,250,250)", height: "100vh" }}>
        <div className="w-9/12 xl:w-7/12 mx-auto">
          <Head>
            <title>MakeChaos</title>
            <link rel="icon" href="/favicon.ico" />
          </Head>

          <main className="container flex">
            <div className="w-1/2 py-48">
              <h1 className="text-5xl mb-0">MakeChaos</h1>
              <h3 className="text-xl font-medium">
                Korkmana gerek yok! Yasal Suç Olduğundan Dolayı Kullandığım
                Teknoloji Bana Şifreni Gösteremiyor ve Ben İsmini Gizlediğin
                Zaman Kim Olduğunu Bulamıyorum
              </h3>
            </div>
            <div className="w-1/2 py-24">
              <div className="bg-white shadow-lg rounded-lg px-4 py-4">
                <SignInput
                  isPassword={false}
                  placeholder="İsim/Soyisim"
                  theRef={name}
                  enterFunc={validate}
                />
                <SignInput
                  isPassword={false}
                  placeholder="Kullanıcı Adı"
                  theRef={username}
                  enterFunc={validate}
                />
                <SignInput
                  isPassword={false}
                  placeholder="Email Adresi"
                  theRef={email}
                  enterFunc={validate}
                />
                <SignInput
                  isPassword={true}
                  placeholder="Şifre"
                  theRef={password}
                  enterFunc={validate}
                />
                <SignInput
                  isPassword={true}
                  placeholder="Şifre Tekrar"
                  theRef={repassword}
                  enterFunc={validate}
                />
                {signErr != null ? (
                  <p
                    className="error -mt-1 mb-2 pl-2 text-red-700 text-lg"
                    id="error"
                  >
                    {signErr}
                  </p>
                ) : null}

                <button
                  style={{ outline: "none" }}
                  className="w-full p-1 px-2 rounded-md bg-blue-500 text-white font-semibold text-2xl select-none hover:bg-blue-600 duration-300 transition-colors"
                  onClick={validate}
                >
                  Kayıt Ol
                </button>
                <p
                  style={{ color: "rgb(60,60,60)" }}
                  className="text-center my-6 text-xl"
                >
                  zaten hesabın var mı
                </p>

                <Link href="/">
                  <button
                    style={{
                      outline: "none",
                    }}
                    className="w-56 block mx-auto p-1 px-2 rounded-md bg-blue-600 text-white font-semibold text-2xl select-none hover:bg-blue-500 duration-300 transition-colors"
                  >
                    Giriş Yap
                  </button>
                </Link>
              </div>
            </div>
          </main>
        </div>
      </div>
    );
}
