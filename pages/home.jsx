import Head from "next/head";
import { useEffect, useState, useRef, useCallback } from "react";
import { observer } from "mobx-react";
import * as userStore from "../store/userStore";

import * as firebase from "firebase/app";
import "firebase/auth";
import "firebase/firestore";

import Message from "../components/Message";
import clsx from "clsx";

// const getMessages = async () => {
//   let messages = [];
//   return new Promise(async (res, rej) => {
//     await firebase
//       .firestore()
//       .collection("users")
//       .get()
//       .then((data) => {
//         data.docs.forEach((doc) => {
//           messages.push(doc.data());
//         });
//       });
//     console.log(messages);
//     res(
//       messages.map((message) => {
//         <div>asd</div>;
//       })
//     );
//   });
// };
// getMessages();

const Home = observer(() => {
  const [isSecure, setIsSecure] = useState(false);
  const [messages, setMessages] = useState(null);
  const message = useRef(null);

  const send = useCallback(() => {
    if (message.current.value != undefined && message.current.value != "") {
      firebase
        .firestore()
        .collection("messages")
        .doc()
        .set({
          sender: isSecure ? "" : userStore.username.get(),
          text: message.current.value,
          time: firebase.firestore.Timestamp.fromDate(new Date()),
        });
      message.current.value = "";
    }
  }, [isSecure]);

  useEffect(() => {
    firebase
      .firestore()
      .collection("messages")
      .orderBy("time", "asc")
      .onSnapshot(
        (docSnapshot) => {
          let newMessages = [];
          docSnapshot.forEach((data) => {
            newMessages.push({ ...data.data(), id: data.id });
          });
          setMessages(newMessages);
        },
        (err) => {
          console.log(`Encountered error: ${err}`);
        }
      );

    window.addEventListener("keypress", (e) => {
      if (e.key != "Enter" && e.key != "Shift") {
        message.current.focus();
      }
    });
  }, []);

  if (typeof window !== "undefined" && userStore.isAuth.get() == false) {
    window.location = "/";
  }

  if (typeof window !== "undefined" && userStore.isAuth.get() == false) {
    window.location = "/";
  }

  if (userStore.isAuth.get() === true)
    return (
      <div style={{ background: "rgb(250,250,250)", height: "100vh" }}>
        <Head>
          <title>MakeChaos</title>
          <link rel="icon" href="/favicon.ico" />
        </Head>
        <div className="fixed right-0 mr-5 mt-2 flex">
          <p
            className="mr-10 cursor-pointer"
            onClick={() => firebase.auth().signOut()}
          >
            Çıkış Yap
          </p>
          <p>{userStore.username.get()}</p>
        </div>

        <main>
          <h1 className="text-4xl ml-16">MakeChaos</h1>

          <div
            className="messages w-8/12 mx-auto relative block pt-6 overflow-y-auto"
            style={{ maxHeight: "82vh" }}
          >
            {messages
              ? messages.map((message) => (
                  <Message
                    key={message.id}
                    sender={message.sender}
                    message={message.text}
                    time={message.time}
                  />
                ))
              : null}
          </div>

          <div className="your-message w-full h-14 mx-auto absolute bottom-0 mb-4 flex justify-center">
            <button
              style={{ outline: "none" }}
              className={clsx("mr-3", isSecure && "font-semibold")}
              onClick={() => {
                setIsSecure(!isSecure);
              }}
            >
              {isSecure ? "Gizli" : userStore.username.get()}
            </button>

            <input
              type="text"
              ref={message}
              className="w-7/12 bg-transparent h-10  outline-none text-xl border-gray-700 border-b-2 px-1"
              placeholder="Yaz ve Gönder"
              onKeyPress={(e) => {
                if (e.key == "Enter") {
                  send();
                }
              }}
            />

            <button style={{ outline: "none" }} className="ml-3" onClick={send}>
              Gönder
            </button>
          </div>
        </main>
      </div>
    );
  else return <div></div>;
});

export default Home;
