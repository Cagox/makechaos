import "../styles.css";

import * as firebase from "firebase/app";

// These imports load individual services into the firebase namespace.
import "firebase/auth";
import "firebase/database";

if (!firebase.apps.length) {
  firebase.initializeApp({
    apiKey: "AIzaSyBKWbyzAeXh6slQDcygvrcKKBYcmBQND-o",
    authDomain: "thefirst-611b6.firebaseapp.com",
    databaseURL: "https://thefirst-611b6.firebaseio.com",
    projectId: "thefirst-611b6",
    storageBucket: "thefirst-611b6.appspot.com",
    messagingSenderId: "431491878614",
    appId: "1:431491878614:web:cb01905ce52b7d61110377",
    measurementId: "G-JXWYTNN6B3",
  });
}

// This default export is required in a new `pages/_app.js` file.
export default function MyApp({ Component, pageProps }) {
  return <Component {...pageProps} />;
}
